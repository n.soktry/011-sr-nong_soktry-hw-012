import React from "react";
import { Form, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const Auth = (props) => {
  return (
      <Form>
        <Form.Row>
          <Col>
            <Form.Control
              placeholder="Username"
              name="username"
              type="text"
              onChange={props.onChange}
            />
          </Col>
          <Col>
            <Form.Control
              placeholder="Password"
              name="password"
              type="password"
              autoComplete="false"
              onChange={props.onChange}
            />
          </Col>
          <Col>
            <Link to="/welcome">
              <Button type="submit" onClick={props.onSubmit}>
                Submit
              </Button>
            </Link>
          </Col>
        </Form.Row>
      </Form>
  );
};

export default Auth;