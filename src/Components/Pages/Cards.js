import React from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

const Cards = (props) => {
  let data = props.data;
  return (
    <div className="col-md-4 col-sm-12">
      <Card>
        <Card.Img variant="top" src={data.img} />
        <Card.Body>
          <Card.Title>{data.title}</Card.Title>
          <Card.Text>{data.description}</Card.Text>
          <Link to={`/post/${data.id}`}>See more</Link>
        </Card.Body>
        <Card.Footer>
          <small>Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </div>
  );
};

export default Cards;
