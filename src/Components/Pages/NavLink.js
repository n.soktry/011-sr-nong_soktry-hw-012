import React from "react";
import { Navbar, Nav, FormControl, Form, Button} from "react-bootstrap";
import { Link } from "react-router-dom";

const NavLink = () => {
  return (
    <Navbar bg="dark" expand="lg">
      <Navbar.Brand as={Link} to="/" style={{ color: "white" }}>
        REACT ROUTER
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/home" style={{ color: "white" }}>
            HOME
          </Nav.Link>
          <Nav.Link as={Link} to="/video" style={{ color: "white" }}>
            VIDEO
          </Nav.Link>
          <Nav.Link as={Link} to="/account" style={{ color: "white" }}>
            ACCOUNT
          </Nav.Link>
          <Nav.Link as={Link} to="/auth" style={{ color: "white" }}>
            AUTH
          </Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button type="submit">Search</Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavLink;