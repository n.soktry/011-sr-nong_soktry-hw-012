import React from "react";
import queryString from "query-string";

const SelectedAccount = (props) => {
  var select = queryString.parse(props.val);
  return select.name !== undefined ? (
    <div>
      <h4>
        The <span style={{color:"red"}}>name</span> in the query string is "{select.name}".
    </h4>
    </div>
  ) : (
    <h4>There is no name in the query string!</h4>
  );
};

export default SelectedAccount;