import React from "react";

const PostContent = (props) => {
  let id = props.match.params.id;
  return <h2>This is the content from post {id}!</h2>;
};

export default PostContent;
