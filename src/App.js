import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavLink from "./Components/Pages/NavLink.js";
import SelectedAccount from "./Components/Pages/AccountPages/SelectedAccount.js";
import MainContent from "./Components/Pages/MainContent.js";
import Account from "./Components/Pages/AccountPages/Account.js";
import WelcomePage from "./Components/Pages/AuthPages/WelcomePage.js";
import PostContent from "./Components/Home/PostContent";
import Home from "./Components/Home/Home";
import Video from "./Components/Pages/VideoPage/Video.js";
import Auth from "./Components/Pages/AuthPages/Auth.js";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      authCheck: false,
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = () => {
    let { username, password } = this.state;
    let check;
    if (username !== "" && password !== "") {
      check = true;
    } else {
      check = false;
      alert("Please input all field");
    }

    this.setState({
      authCheck: check,
      username: "",
      password: "",
    });
  };

  render() {
    return (
      <div className="App">
        <Router>
          <NavLink></NavLink>
          <div className="container mt-4">
            <Switch>
              <Route path="/" exact component={MainContent} />
              <Route path="/home" component={Home} />
              <Route path="/video" component={Video} />
              <Route path="/account" exact component={Account} />
              <Route path="/account" component={SelectedAccount} />
              <Route
                path="/auth"
                render={() => (
                  <Auth
                    onSubmit={this.handleSubmit.bind(this)}
                    onChange={this.handleChange}
                  />
                )}
              />
              <Route path="/post/:id" component={PostContent} />
              <Route
                path="/welcome"
                render={() => <WelcomePage auth={this.state.authCheck} />}
              />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;